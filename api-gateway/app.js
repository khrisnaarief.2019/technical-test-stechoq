require ('dotenv').config();
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const krsRouter = require ('./routes/krs');
const mahasiswaRouter = require ('./routes/mahasiswa');
const matkulRouter = require ('./routes/matkul');
const prodiRouter = require ('./routes/prodi');

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/krs', krsRouter);
app.use('/mahasiswa', mahasiswaRouter);
app.use('/matkul', matkulRouter);
app.use('/prodi', prodiRouter);

module.exports = app;
